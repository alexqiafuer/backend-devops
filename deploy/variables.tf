variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "backend-api-devops"
}

variable "contact" {
  default = "myemail@example.com"
}

# Username for database, no default is provided intentionally. 
# Provide values for username and password as environ variable or in terminal
variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}